/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package removethebadstaff;

import java.awt.Color;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author H303565
 */
public class GUI extends javax.swing.JFrame {

    public GUI() {
        initComponents();
    }

    @SuppressWarnings("unchecked")

    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Path: ");
        jTextField1.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                warn();
            }

            public void removeUpdate(DocumentEvent e) {
                warn();
            }

            public void insertUpdate(DocumentEvent e) {
                warn();
            }

            public void warn() {
                PrintWriter writer;
                String actual_path = jTextField1.getText();
                jComboBox1.removeAllItems();

                File curDir = new File(actual_path);
                try {
                    if (curDir.isDirectory()) {
                        File[] filesList = curDir.listFiles();
                        for (File f : filesList) {
                            if (f.isFile()) {
                                jComboBox1.addItem(f.getName());
                            }
                        }

                        writer = new PrintWriter("last_path.txt", "UTF-8");
                        writer.println(curDir);
                        writer.close();

                        jTextField1.setBackground(Color.GREEN);
                        if (curDir == null || curDir.listFiles().length == 0) {
                            jTextField1.setBackground(Color.RED);
                            jComboBox1.addItem("Please enter another path!");
                        }
                    } else {
                        jTextField1.setBackground(Color.RED);
                        jComboBox1.addItem("Please enter correct path!");
                    }
                } catch (Exception a) {
                    jTextField1.setBackground(Color.RED);
                    jComboBox1.addItem("Please enter correct path!");
                }

            }
        });

        //fill_path();
        //jTextField1.setBackground(Color.GREEN);
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("File: ");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{""}));

        jButton1.setText("Remove");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    String path = jTextField1.getText();
                    String selected_file = jComboBox1.getSelectedItem().toString();
                    File curDir = new File(jTextField1.getText());
                    String error_lines = "";

                    if (curDir.isDirectory()) {
                        toEditFile = path + "\\" + selected_file;
                    } else if (curDir.isFile()) {
                        toEditFile = path;
                    }

                    FileInputStream fstream;
                    BufferedReader br = null;

                    try {
                        StringBuilder sb = new StringBuilder();
                        fstream = new FileInputStream(toEditFile);
                        br = new BufferedReader(new InputStreamReader(fstream));
                        String line, line_old;
                        PrintWriter writer;
                        ArrayList<String> changed_lines = new ArrayList<>();
                        int line_num = 1;

                        writer = new PrintWriter(path + "\\" + selected_file + ".trim", "UTF-8");

                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                            sb.append(System.lineSeparator());

                            line_old = line;
                            line = line.replaceAll("\t", "        ");
                            line = line.replaceAll("[^\\x00-\\x7F]", " ");
                            line = line.replaceAll("\\p{C}", " ");

                            if (!line.equals(line_old)) {
                                changed_lines.add("" + line_num);
                                error_lines = error_lines.concat(line_num + ", ");
                                if (line_num % 30 == 0) {
                                    error_lines = error_lines.concat("\n");
                                }
                            }

                            line_num++;
                            writer.println(line);
                        }

                        br.close();
                        writer.close();

                        JFrame parent = new JFrame();

                        if (error_lines.length() != 0) {
                            JOptionPane.showMessageDialog(parent, "The follow lines had special characters:\n" + error_lines);
                            File file = new File(path + "\\" + selected_file + ".trim");
                            Desktop desktop = Desktop.getDesktop();
                            if (file.exists()) {
                                desktop.open(file);
                            }

                        } else {
                            Files.deleteIfExists(Paths.get(path + "\\" + selected_file + ".trim"));
                            JOptionPane.showMessageDialog(parent, "No special characters found!");
                        }

                    } catch (Exception a) {
                        System.out.println(a.toString() + "aaaaa");
                    }

                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(rootPane, "Please insert a correct path: " + toEditFile, "Module name error", 1);
                    System.out.println(ex.toString());
                }
            }

        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(55, 55, 55)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jComboBox1, 0, 668, Short.MAX_VALUE)
                                                        .addComponent(jTextField1)))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(36, 36, 36)
                                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(39, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(43, 43, 43)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jTextField1)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(31, 31, 31)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jComboBox1)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(70, 70, 70)
                                .addComponent(jButton1)
                                .addContainerGap(293, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewJFrame().setVisible(true);
            }
        });
    }

    public void fill_path() {

        File file = new File("last_path.txt");

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String path;
            path = br.readLine();
            jTextField1.setText(path);
        } catch (FileNotFoundException | UnsupportedEncodingException | NullPointerException ex) {
            jTextField1.setText("G:\\777_APUCSW_RC\\Software\\source\\Arinc\\test_proc\\host");
            System.out.println(ex.toString());
        } catch (IOException ex) {
            Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField1;
    private String toEditFile = "";
    // End of variables declaration                   
}
